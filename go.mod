module gitlab.ulb.tu-darmstadt.de/fid-bau/microservices/fileconv

go 1.18

require (
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220318055525-2edf467146b5 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20220317150908-0efb43f6373e // indirect
	google.golang.org/grpc v1.45.0
	google.golang.org/protobuf v1.27.1
)

require github.com/mgutz/str v1.2.0

require github.com/golang/protobuf v1.5.2 // indirect
