package internal

import (
	"log"
	"os"
	"strconv"
)

var NumCPUs = envVarAsInt("NUM_CPUS", 1)
var NumCPUsIFC = envVarAsInt("NUM_CPUS_IFC", 1)
var storageDir = envVar("STORAGE_DIR", "/workbench-filestore")

func envVarAsInt(key string, defaultValue int) int {
	if val, present := os.LookupEnv(key); present {
		res, err := strconv.Atoi(val)
		if err != nil {
			log.Printf("warning: env var '%s' with value '%s' is not an integer. using default %d\n", key, val, defaultValue)
			return defaultValue
		} else {
			return res
		}
	}
	return defaultValue
}

func envVar(key string, defaultValue string) string {
	if val, present := os.LookupEnv(key); present {
		return val
	}
	return defaultValue
}
