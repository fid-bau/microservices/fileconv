package internal

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"time"

	"github.com/mgutz/str"

	pb "gitlab.ulb.tu-darmstadt.de/fid-bau/microservices/fileconv/pkg/api/v1"
)

type Executable struct {
	Executable  string
	DefaultArgs string
}

var Python = Executable{Executable: "python3"}
var IfcConvert = Executable{Executable: "IfcConvert", DefaultArgs: fmt.Sprintf("-qy --threads %d", NumCPUsIFC)}
var ImageMagickConvert = Executable{Executable: "convert"}
var PotreeConverter = Executable{Executable: "/usr/local/bin/PotreeConverter"}
var CloudCompare = Executable{Executable: "CloudCompare", DefaultArgs: "-SILENT -C_EXPORT_FMT LAS"}

func Exec(ctx context.Context, e Executable, input *pb.ExecInput) (output *pb.ExecOutput, err error) {
	// execute command with given params
	options := make([]string, 0)
	if e.DefaultArgs != "" {
		options = append(options, strings.Split(e.DefaultArgs, " ")...)
	}
	// this keeps tokens wrapped by quotation marks as a single token (needed for files with spaces in their name)
	options = append(options, str.ToArgv(input.Args)...)

	cmd := exec.CommandContext(ctx, e.Executable, options...)
	cmd.Dir = storageDir
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	log.Printf("exec: '%+v'\n", cmd)
	start := time.Now()
	defer func() {
		if err == nil {
			log.Printf("successful exec '%+v' took %s\n", cmd, time.Since(start))
		} else {
			log.Printf("failed exec '%+v' took %s. error was: %v\n", cmd, time.Since(start), err)
		}
	}()
	if err = cmd.Run(); err != nil {
		err = fmt.Errorf("%s %s: %w", out.String(), stderr.String(), err)
		return
	}
	fmt.Println("stdout", out.String())
	fmt.Println("stderr", stderr.String())
	output = &pb.ExecOutput{}
	return
}
