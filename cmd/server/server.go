package main

import (
	"context"
	"flag"
	"log"
	"net"
	"strconv"

	"google.golang.org/grpc"

	"gitlab.ulb.tu-darmstadt.de/fid-bau/microservices/fileconv/internal"
	pb "gitlab.ulb.tu-darmstadt.de/fid-bau/microservices/fileconv/pkg/api/v1"
)

func main() {
	var port int

	// flags declaration using flag package
	flag.IntVar(&port, "p", 3000, "Specify port to listen on")
	flag.Parse()

	listener, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	pb.RegisterFileConvServer(s, &server{})
	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

type server struct {
	pb.UnimplementedFileConvServer
}

func (server) ExecPython(ctx context.Context, input *pb.ExecInput) (*pb.ExecOutput, error) {
	return internal.Exec(ctx, internal.Python, input)
}

func (server) ExecIfcConvert(ctx context.Context, input *pb.ExecInput) (*pb.ExecOutput, error) {
	return internal.Exec(ctx, internal.IfcConvert, input)
}

func (server) ExecImageMagickConvert(ctx context.Context, input *pb.ExecInput) (*pb.ExecOutput, error) {
	return internal.Exec(ctx, internal.ImageMagickConvert, input)
}

func (server) ExecPotreeConverter(ctx context.Context, input *pb.ExecInput) (*pb.ExecOutput, error) {
	return internal.Exec(ctx, internal.PotreeConverter, input)
}

func (server) ExecCloudCompare(ctx context.Context, input *pb.ExecInput) (*pb.ExecOutput, error) {
	return internal.Exec(ctx, internal.CloudCompare, input)
}
