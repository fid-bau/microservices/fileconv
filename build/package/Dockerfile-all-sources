FROM golang:alpine AS build-app
WORKDIR /app
RUN apk update && apk add --no-cache protoc git && \
    go install google.golang.org/protobuf/cmd/protoc-gen-go@latest && \ 
    go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
ADD . .
RUN go mod download && go mod verify && \
    scripts/protoc-gen.sh && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" cmd/server/server.go

FROM ubuntu:20.04 as build-ifc
ENV DEBIAN_FRONTEND=noninteractive
ARG IFC_VERSION="v0.7.0"
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt-get -y install git gcc g++ autoconf bison make cmake libfreetype6-dev mesa-common-dev libffi-dev libfontconfig1-dev python3.9
WORKDIR /source/IfcOpenShell
RUN git clone --recursive https://github.com/IfcOpenShell/IfcOpenShell.git . && git checkout ${IFC_VERSION}
WORKDIR /source/IfcOpenShell/nix
RUN CFLAGS=-O3 CXXFLAGS=-O3 BUILD_CFG=Release python3 build-all.py -ENABLE_BUILD_OPTIMIZATIONS IfcConvert OpenCOLLADA

#FROM ubuntu:20.04 as build-blender
FROM debian:bullseye-slim as build-blender
ENV DEBIAN_FRONTEND=noninteractive
ARG BLENDER_VERSION="blender-v3.0-release"
RUN apt-get update && apt-get -y install build-essential git subversion cmake libx11-dev libxxf86vm-dev libxcursor-dev libxi-dev libxrandr-dev libxinerama-dev libglew-dev
WORKDIR /source/blender-git/blender
RUN git clone https://git.blender.org/blender.git . && \
    git checkout ${BLENDER_VERSION}
WORKDIR /source/blender-git/lib
RUN svn checkout https://svn.blender.org/svnroot/bf-blender/trunk/lib/linux_centos7_x86_64
WORKDIR /source/blender-git/blender

# RUN apt-get update && apt-get upgrade -y --no-install-recommends \
# 	autoconf \
# 	automake \
# 	bison \
# 	ca-certificates \
# 	cmake \
# 	diffutils \
# 	flex \
# 	g++ \
# 	gawk \
# 	gcc \
# 	gettext \
# 	git \
# 	libbz2-dev \
# 	libc-dev \
# 	libfreetype6-dev \
# 	libsdl1.2-dev \
# 	libsqlite3-dev \
# 	libssl-dev \
# 	libtinyxml-dev \
# 	libtool \
# 	libxcursor-dev \
# 	libxi-dev \
# 	libxinerama-dev \
# 	libxml2-dev \
# 	libxrandr-dev \
# 	libyaml-cpp-dev \
# 	make \
#     meson \
# 	nasm \
#     ninja-build \
# 	patchutils \
# 	tcl-dev \
# 	wget \
# 	yasm && \
# 	rm -rf /var/lib/apt/lists/* && \
# 	update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# ARG BLENDER_SRC_DIR="/source/blender-git/blender"
# ARG BLENDER_BUILD_DIR="/source/blender-git/build-linux"
# ARG LIB_BUILD_DIR="/source/blender-git/build-lib-linux"
# ARG LIB_DIR="/source/blender-git/lib/linux_x86_64"

# WORKDIR ${LIB_BUILD_DIR}
# RUN  cmake ${BLENDER_SRC_DIR}/build_files/build_environment -DLIBDIR=${LIB_DIR} -DDOWNLOAD_DIR=${LIB_BUILD_DIR}/Download -DHARVEST_TARGET=${LIB_DIR}
# RUN make install

#RUN apt-get install -y autoconf automake libtool yasm tcl ninja-build meson python3-mako bison pkg-config
#RUN mkdir /source/blender-git/lib && make deps
ENV PYTHON="/source/blender-git/lib/linux_centos7_x86_64/python/bin/python3.9"
ENV PYTHONHOME="/source/blender-git/lib/linux_centos7_x86_64/python/"
RUN make update
RUN CMAKE_INSTALL_PREFIX=/usr BUILD_CMAKE_ARGS="-DWITH_INSTALL_PORTABLE=OFF -DWITH_DRACO=ON -DWITH_MEM_JEMALLOC=OFF -DWITH_INTERNATIONAL=OFF -DWITH_X11_XF86VMODE=ON" make headless

# install Blender's rhino addon
#ENV BLENDER_INSTALLDIR="/source/blender-git/build_linux_headless/bin"
ENV BLENDER_INSTALLDIR="/usr/local/share/blender"
ENV PYTHON="${BLENDER_INSTALLDIR}/3.0/python/bin/python3.9"
ENV PYTHONHOME="${BLENDER_INSTALLDIR}/3.0/python/"
#RUN ${PYTHON_EXEC} -m ensurepip && \
#     ${PYTHON_EXEC} -m pip install -U pip rhino3dm
WORKDIR /source/rhino3dm
RUN git clone --recurse-submodules https://github.com/mcneel/rhino3dm.git .
RUN ${PYTHON} -m ensurepip && \
     ${PYTHON} -m pip install -U pip setuptools
RUN cp -r /source/blender-git/lib/linux_centos7_x86_64/python/include/python3.9 ${BLENDER_INSTALLDIR}/3.0/python/include/
RUN ${PYTHON} setup.py bdist
WORKDIR /source/import_3dm
RUN git clone https://github.com/jesterKing/import_3dm . &&\
    cp -r ./import_3dm ${BLENDER_INSTALLDIR}/3.0/scripts/addons/
RUN cp -r /source/rhino3dm/build/lib.linux-x86_64-3.9/rhino3dm ${PYTHONHOME}/lib/python3.9/site-packages/

FROM debian:bullseye-slim
ENV DEBIAN_FRONTEND=noninteractive
ENV USER=user
ENV UID=10001
#ARG IMAGE_TOOLS="imagemagick potrace"
ARG IMAGE_TOOLS="libx11-6 libxext6 libglu1"
RUN adduser --disabled-password --gecos "" --home "/tmp" --shell "/sbin/nologin" --no-create-home --uid "${UID}" "${USER}" && \
    apt-get update && apt-get -y install $IMAGE_TOOLS
COPY --from=build-app --chown=${USER}:${USER} /app/server /app/
COPY --from=build-ifc --chown=${USER}:${USER} /source/IfcOpenShell/build/Linux/x86_64/install/ifcopenshell/bin/IfcConvert /usr/local/bin/
#COPY --from=build-blender --chown=${USER}:${USER} /source/blender-git/build_linux_headless/bin/ /usr/local/bin/
COPY --from=build-blender --chown=${USER}:${USER} /usr/local/share/blender/3.0/ /usr/local/bin/3.0
COPY --from=build-blender --chown=${USER}:${USER} /usr/local/bin/* /usr/local/bin/
ENV PYTHONHOME="/usr/local/bin/3.0/python/"
 
# cleanup
#RUN apt-get -y purge git && \
#    apt-get -y autoremove && \
#    rm -rf /var/lib/apt/lists/* /tmp/*

WORKDIR /app
#USER ${USER}:${USER}

RUN echo '\
import bpy\n\
bpy.ops.wm.read_factory_settings(use_empty=True)\n\
bpy.ops.wm.save_homefile()\n\
bpy.ops.preferences.addon_enable(module="import_3dm")\n\
bpy.ops.preferences.addon_enable(module="io_import_dxf")\n\
bpy.ops.wm.save_userpref()\n'\
 | blender -b -noaudio --python-console

ADD Excavator.3dm .
ADD blender_convert.py .
ADD airplane.ply .

EXPOSE 3000
VOLUME [ "/workbench-filestore" ]
ENTRYPOINT ["./server"]